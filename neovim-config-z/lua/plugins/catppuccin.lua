return {
  {
    "catppuccin/nvim",
    name = "catppuccin",
    opts = {
      flavour = "latte",
      dim_inactive = { enabled = true, percentage = 0.25 },
    },
  },
}
