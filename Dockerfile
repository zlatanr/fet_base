FROM ubuntu:22.04

SHELL ["/bin/bash","-c","-l"]

ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8

RUN apt-get update && apt-get upgrade -y  && \ 
       DEBIAN_FRONTEND=noninteractive apt-get -y install unzip build-essential \
       pip zlib1g-dev \
       pass tmux mosh bison curl git pv ninja-build wget flex patch lzma \
       libpng-dev p7zip-full sudo bash-completion apt-utils xclip bzip2 libncursesw5-dev \
       pkg-config automake libtool autoconf global libtool-bin gettext p7zip-full \
       locales locales-all software-properties-common libncurses5-dev ccache \
       libcurl4-openssl-dev distcc command-not-found traceroute iputils-ping \
       netcat-traditional gdb-multiarch nmap tcpdump iperf3 net-tools\
       bc libpcap-dev libssl-dev libmp3lame-dev libsoundio-dev libgmp-dev dnsutils\
       genisoimage rxvt-unicode libusb-1.0 \
       libopus0 libgtk-3-0 libepoxy0 libsasl2-2 libbz2-dev \
       libgstreamer-plugins-base1.0 liborc-0.4-0 libgbm1 \
       geany geany-plugin-addons geany-plugin-autoclose geany-plugin-codenav \
       geany-plugin-defineformat libreadline-dev libgtk-3-dev fd-find \
       nodejs npm lua5.4 bubblewrap opam ripgrep &&\
       apt-get clean

RUN mkdir -p /opt1/ && \
       curl -sL https://github.com/Kitware/CMake/releases/download/v3.29.2/cmake-3.29.2-Linux-x86_64.tar.gz | tar zxf - --strip-components=1 -C /opt1 &&\
       cd /tmp && curl -sLO https://go.dev/dl/go1.22.2.linux-amd64.tar.gz && tar -C /opt1 -xzf go1.22.2.linux-amd64.tar.gz &&\
       opam init --disable-sandboxing -y && eval $(opam env --switch=default) && opam install ocaml-lsp-server utop -y #fix location installation

RUN pip install neovim neovim-remote meson asciinema

COPY bin /opt1/bin/
COPY script.sh /opt1/
COPY tmux.conf /opt1/.tmux.conf
COPY inputrc  /opt1/.inputrc
COPY bashrc  /opt1/.bashrc
COPY neovim-config-z /opt1/nvim-cfg/nvim

RUN mkdir -p nvim && \
       export PATH=$PATH:/opt1/bin && cd /tmp && git clone --depth 1 --branch v0.9.5 https://github.com/neovim/neovim && cd neovim && \
       make CMAKE_BUILD_TYPE=Release CMAKE_EXTRA_FLAGS="-DCMAKE_INSTALL_PREFIX=/opt1" && make install && \
       chmod 777 /opt1/nvim-cfg/nvim/lazy-lock.json && \
       XDG_CONFIG_HOME=/opt1/nvim-cfg nvim +qall --headless

#Cleanup
RUN rm -rf /tmp/*

